## Project Changelog

<a name="x.y.z"></a>

## 0.0.2

### Features

- Added New TimeSlots Modal
- Added Osteopaths Carousel
- Added isOsteopath Guard Function
- Added Appointment List for Users

### Bug Fixes

- Images size fixed
-

### ⚠ BREAKING CHANGES

- Removed the Logout Button Instead added a Dropdown for showing options
- Added Time Availability UI to board
