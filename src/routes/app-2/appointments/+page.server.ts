import { db } from "$lib/server/db";

export const load = async () => {
  const appointments = db.query.appointment.findMany({ with: { osteopath: { with: { user:true } }, user: true } })
  return {
    appointments
  }
};
