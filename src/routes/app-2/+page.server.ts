import { dev } from "$app/environment";
import { auth, googleAuth } from "$lib/server/lucia";
import { fail, redirect } from "@sveltejs/kit";
import type { Actions, PageServerLoad } from './$types';
import { Temporal } from "@js-temporal/polyfill";
import { appointment } from "$lib/server/db/schema";
import { db } from "$lib/server/db";
import { eq } from "drizzle-orm";

export const load: PageServerLoad = async () => {
  const osteopaths = db.query.osteopath.findMany({ with: { user: true } })
  const appointments = db.query.appointment.findMany({ with: { osteopath: { with: {user:true} } } })
  return { appointments, osteopaths }
};

export const actions: Actions = {
  book: async ({ request }) => {
    console.log("RUNNING BOOKING ACTION")
    const formData = await request.formData();
    const userId = formData.get('userId')
    const osteopathId = formData.get('osteopathId')
    const appointmentId = formData.get('appointmentId');
    console.log(userId)
    console.log(osteopathId)
    console.log(appointmentId)
    if(userId && osteopathId && appointmentId) {
      const res = await db.update(appointment).set({
        userId: userId.toString(),    
      }).where(eq( appointment.id, appointmentId.toString() )).returning()
      console.log("RESPONSE ",res)
    }
  }
};