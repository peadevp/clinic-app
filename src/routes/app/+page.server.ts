import { db } from "$lib/server/db";
import { appointment, osteopath, user } from "$lib/server/db/schema";
import groupBy from "just-group-by";
import type { PageServerLoad } from "./$types";
import { and, asc, eq, isNull, gte } from "drizzle-orm";
import { Temporal } from "@js-temporal/polyfill";

export const load: PageServerLoad = async () => {
  const today = Temporal.Now.plainDateISO().toLocaleString('en',{
    year: 'numeric',
    month: '2-digit',
    day:'2-digit'
  });
  const [month,day,year] = today.split('/');
  const t = `${year}-${month}-${day}`
  /**
   * Query is loading appointments
   * Appointments with Empty User Slot
   * In Ascending Order of Dates, Shows the latest appointments
   */

  const data = await db.select().from(appointment).where(and(isNull(appointment.userId),gte(appointment.date,t))).innerJoin(osteopath, eq(appointment.osteopathId,osteopath.id)).innerJoin(user,eq(user.id,osteopath.userId)).orderBy(asc(appointment.date))

  const appointments = data.map((appointment) => ({
    ...appointment.appointment,
    osteopath: {
      ...appointment.osteopath,
      user: appointment.user,
    }
  }));
  const byosteopathID = groupBy(appointments, (appointment) => appointment.osteopathId);
	const bydates = groupBy(appointments, (appointment) => appointment.date as string);
  
  return { by: { osteopath:byosteopathID, dates:bydates } }
};
