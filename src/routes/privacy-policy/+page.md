# Privacy Policy

Last updated on October 30, 2023

## Introduction

Clinic-App we is committed to protecting the privacy of users of our date-time scheduling web app for osteopaths, which integrates with Google Calendar Services.
This Privacy Policy outlines our practices regarding the collection, use, and disclosure of personal and non-personal information from users of our app.<br/>
**By using our app, you consent to the practices described in this Privacy Policy.**

## Information We Collect

We may collect the following types of information:

- User-Provided Information: When you register for our app or schedule appointments, you may provide personal information, including but not limited to your name, email address, phone number, and clinic information.
- Google Calendar Information: To provide scheduling and appointment management features, our app may access and integrate with your Google Calendar account. This may include accessing your calendar events, appointments, and associated data. We do not store your Google Calendar login credentials.
- Usage Data: We don’t collect data on how you use our app, yet we collect the information which includes appointments scheduled and patient history to provide you with better user experience
- Log Data: When you use our app, we may automatically collect log data, such as your device's IP address, browser type, pages visited, and other usage information.

## How We Use Your Information

We use the information we collect for the following purposes:

- Appointment Scheduling: To schedule, manage, and provide reminders for appointments on behalf of doctors and clinics.
- Communication: To communicate with you, including sending appointment reminders, updates, and notifications.
- Improvement: To improve our app, services, and user experience.
- Analytics: To analysis app usage and performance to make data-driven decisions.

## Cookies

A cookie is a small amount of data, which often includes an anonymous unique identifier, which is sent to your browser from a web site’s computers and stored on your computer’s hard drive.
You can control how websites use cookies by configuring the privacy settings within your browser. Note that if you disable cookies entirely, Clinic Web App not function properly.

## Information Sharing

We do not share your personal information with third parties except in the following cases:

- With Your Consent: If you grant explicit consent for information sharing.
- Legal Compliance: When required to comply with applicable laws, regulations, or legal processes.

## Security

We implement reasonable security measures to protect your personal information. However, no data transmission or storage can be guaranteed to be 100% secure.

## Your Choices

You have choices regarding the information we collect and how it is used:

- Access and Modification: You can access and modify your personal information within the app.
- Unsubscribe: You can opt-out of receiving certain emails or notifications from us.

## Changes to Privacy Policy

We may periodically update this policy. We encourage you to periodically review our website for the latest information on our privacy practices.

## Acceptance of this policy

You acknowledge that you have read this Policy and agree to all its terms and conditions. By using the Website you agree to be bound by this Policy. If you do not agree to abide by the terms of this Policy, you are not authorised to use or access the Website.

## Contacting Us

Thank you for reading our Privacy Policy. If you have any questions, concerns, or requests regarding this Privacy Policy or the handling of your information, please contact us at sukhpreetben10@gmail.com
