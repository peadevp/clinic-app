---
title: What is Osteopathy
description: Osteopathy is based on the idea that all the body's systems are interrelated. Osteopaths focus on treating the whole person.
date: '2023-4-14'
categories:
  - sveltekit
  - svelte
published: true
---

The practice of osteopathy began in the United States in 1874. Osteopathy was founded by **Andrew Taylor Still**, a 19th-century American physician (MD)

According to the American Osteopathic Association (AOA) states that the four major principles of osteopathic medicine are the following:

- The body is an integrated unit of mind, body, and spirit.
- The body possesses self-regulatory mechanisms, having the inherent capacity to defend, repair, and remodel itself.
- Structure and function are reciprocally interrelated.
- Rational therapy is based on consideration of the first three principles.

Many of osteopathic medicine's manipulative techniques are aimed at reducing or eliminating the impediments to proper structure and function so the **_self-healing mechanism can assume its role in restoring a person to health._**
