Tools & Libraries Setup:

- [x] SvelteKit
- [x] Drizzle
- [x] CockRoachDB
- [x] Tailwindcss
- [x] TypeScript
- [x] Lucia
- [x] Google Auth
- [x] UI Library
- [x] Forms Solution
- [x] Deployment Vercel
